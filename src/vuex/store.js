import axios from 'axios';
import { createStore } from 'vuex'

const store = createStore({
    state: {
        users: []
    },
    getters: {
        USERS(state) {
            return state.users;
        }
    },
    mutations: {
        SET_USERS_TO_STATE: (state, users) => {
            state.users = users;
        }
    },
    actions: {
        GET_USERS_FROM_API({commit}) {
            axios.get('https://jsonplaceholder.typicode.com/users')
            .then((users) => {
                commit('SET_USERS_TO_STATE', users.data)
            })
            .catch((error) => {
                console.log(error)
            })
        }
    }
})

export default store;