import {createApp} from 'vue'
import App from './App.vue'
import store from './vuex/store'
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js"



const app = createApp(App);
app.use(store);
app.mount('#app')